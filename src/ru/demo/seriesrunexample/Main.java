package ru.demo.seriesrunexample;

/**
 * Главный класс для запуска программы
 *
 * @author Kotelnikova E.V. group15it20
 */
public class Main {
    public static void main(String[] args) {
        SeriesRunExample.example();
    }
}
