package ru.demo.eggorchicken;

/**
 * Класс реализует спор между двумя оппонентами на тему, что появилось первым Курица или Яйцо
 *
 * Kotelnikova E.V. group 15it20
 */
public class ChickenVoice {
    static EggVoice eggVoice;

    public static void main(String[] args) {
        eggVoice = new EggVoice();
        System.out.println("Спор начат...");
        eggVoice.start();

        for (int i = 0; i < 5; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {

            }
            System.out.println("Курица!");
        }
        if (eggVoice.isAlive()) {    //Если оппонент еще не сказал последнее слово
            try {
                eggVoice.join();    //Подождать пока оппонент закончит высказываться.
            } catch (InterruptedException e) {

            }
            System.out.println("Первым появилось яйцо!");
        } else {    //если оппонент уже закончил высказываться
            System.out.println("Первой появилась курица!");
        }
        System.out.println("Спор закончен!");
    }
}