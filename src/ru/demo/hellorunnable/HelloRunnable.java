package ru.demo.hellorunnable;

import java.net.ServerSocket;

/**
 * Реализация интерфейса Runnable
 * <p>
 * Интерфейс Runnable определяет один метод run,
 * предназначеннй для размещения кода, исполняемого в потоке.
 * Runnable - объект пересылается в конструктор Thread
 * и с помощью метода start() поток запускается
 */
public class HelloRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("Hello from a thread!");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            System.out.println("Поток завершен");
        }
        System.out.println("Этот поток завершён");
    }

    public static void main(String[] args) {
        (new Thread(new HelloRunnable())).start();
        System.out.println("Hello from main Thread!");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            System.out.println("Поток завершен");
        }
        System.out.println("Главный поток завершён");

        Thread thread = Thread.currentThread();
        thread.setName("Новый поток");
        System.out.println("Имя потока: " + thread.getName());

    }
}
