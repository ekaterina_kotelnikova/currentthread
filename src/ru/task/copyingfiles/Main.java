package ru.task.copyingfiles;

/**
 * Демонстрационный класс для запуска программы, реализующей копирование данных
 * из одного файла в другой
 *
 * @author Kotelnikova E.V. group15it20
 */

public class Main {
    private static final String INPUT_FILE = "src\\ru\\task\\copyingfiles\\input.txt";
    private static final String OUTPUT_FILE_1 = "src\\ru\\task\\copyingfiles\\firstoutput.txt";
    private static final String OUTPUT_FILE_2 = "src\\ru\\task\\copyingfiles\\secondoutput.txt";
    private static final String OUTPUT_FILE_3 = "src\\ru\\task\\copyingfiles\\thirdoutput.txt";
    private static final String OUTPUT_FILE_4 = "src\\ru\\task\\copyingfiles\\fourthoutput.txt";

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Параллельное копирование");
        final long beforeParal = System.currentTimeMillis();
        parallelCopy();
        final long afterParal = System.currentTimeMillis();
        System.out.printf("Параллельное копирование заняло времени: %d ", (afterParal - beforeParal), "\n");

        System.out.println("Последовательное копирование");
        final long beforeCons = System.currentTimeMillis();
        consistentCopy();
        final long afterCons = System.currentTimeMillis();
        System.out.printf("Последовательное копирование заняло времени: %d ", (afterCons - beforeCons), "\n");

    }

    /**
     * Метод для последовательного копирования файлов
     * @throws InterruptedException исключение прерывания потока
     */
    private static void consistentCopy() throws InterruptedException {
        CopyingThread thirdFile = new CopyingThread(INPUT_FILE, OUTPUT_FILE_3);
        CopyingThread fourthFile = new CopyingThread(INPUT_FILE, OUTPUT_FILE_4);

        thirdFile.start();
        thirdFile.join();

        fourthFile.start();
        fourthFile.join();
    }

    /**
     * Метод для параллельного копирования файлов
     * @throws InterruptedException исключение прерывания потока
     */
    private static void parallelCopy() throws InterruptedException {
        CopyingThread firstFile = new CopyingThread(INPUT_FILE, OUTPUT_FILE_1);
        CopyingThread secondFile = new CopyingThread(INPUT_FILE, OUTPUT_FILE_2);

        firstFile.start();
        secondFile.start();

        firstFile.join();
        secondFile.join();
    }
}
