package ru.task.copyingfiles;

import java.io.*;

/**
 * Класс, реализующий копирование данных из одного файла в другой
 *
 * @author Kotelnikova E.V. group15it20
 */
public class CopyingThread extends Thread {
    private String nameInput;
    private String nameOutput;

    CopyingThread(String nameInput, String nameOutput) {
        this.nameInput = nameInput;
        this.nameOutput = nameOutput;
    }

    /**
     * Метод для построчного копирования данных из одного файла в другой
     */
    public void run() {
        try (BufferedReader reader = new BufferedReader(new FileReader(nameInput));
             BufferedWriter writer = new BufferedWriter(new FileWriter(nameOutput))) {
            String str;
            while ((str = reader.readLine()) != null) {
                writer.write(str + "\n");
                System.out.println(str);
            }
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
