package ru.task.readerthread;

import java.io.*;

/**
 * Класс для создания объекта класса Thread для записи строк в отдельный файл
 *
 * @author Kotelnikova E.V. group15it20
 */
public class ReaderThread extends Thread {
    private BufferedReader input;
    private static BufferedWriter output;
    private static SynchronizedMutex mutex;

    ReaderThread(BufferedReader input, BufferedWriter output, SynchronizedMutex mutex) {
        this.input = input;
        this.output = output;
        this.mutex = mutex;
    }

    @Override
    public void run() {
        String str;
        try {
            while ((str = input.readLine()) != null) {
                try {
                    mutex.acquire();
                    System.out.println(str);
                    output.write(str + "\n");
                    mutex.release();
                    output.flush();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            mutex.leave();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
