package ru.task.readerthread;

/**
 * Класс с реализаций основных методов для мьютекса:
 * acquire
 * release
 * leave
 *
 * @author Kotelnikova E.V. group15it20
 */
public class SynchronizedMutex {
    private Thread curOwner = null;
    private Thread lastOwner = null;
    private int threadCount = 2;

    /**
     * Метод для получения разрешения у мьютекса для запуска потока
     *
     * @throws InterruptedException
     */
    public synchronized void acquire() throws InterruptedException {
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        while ((curOwner != null || lastOwner == Thread.currentThread()) && threadCount > 1) {
            wait();
        }
        curOwner = Thread.currentThread();
    }

    /**
     * Метод для освобождения мьютекса и присвоения ему нового "владельцаЭ
     * (передаём разрешение на доступ другому потоку)
     */
    public synchronized void release() {
        if (curOwner == Thread.currentThread()) {
            curOwner = null;
            lastOwner = Thread.currentThread();
            notify();
        } else {
            throw new IllegalStateException("not owner of mutex");
        }
    }

    /**
     * Метод для прекращения действия потока
     */
    public synchronized void leave() {
        threadCount--;
    }
}
