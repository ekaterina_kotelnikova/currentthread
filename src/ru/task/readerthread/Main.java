package ru.task.readerthread;

import sun.awt.Mutex;

import java.io.*;
import java.util.concurrent.Semaphore;

/**
 * Класс для поочерёдной записи строк из двух файлов в один
 *
 * @author Kotelnikova E.V. group15it20
 */
public class Main {
    private static final String INPUT_FILE_1 = "src\\ru\\task\\readerthread\\input.txt";
    private static final String INPUT_FILE_2 = "src\\ru\\task\\readerthread\\input2.txt";
    private static final String OUTPUT_FILE = "src\\ru\\task\\readerthread\\output.txt";

    public static void main(String[] args) throws IOException, InterruptedException {
        try (BufferedReader input1 = new BufferedReader(new FileReader(INPUT_FILE_1));
             BufferedReader input2 = new BufferedReader(new FileReader(INPUT_FILE_2));
             BufferedWriter output = new BufferedWriter(new FileWriter(OUTPUT_FILE))) {
            SynchronizedMutex mutex = new SynchronizedMutex();

            ReaderThread thread1 = new ReaderThread(input1, output, mutex);
            ReaderThread thread2 = new ReaderThread(input2, output, mutex);

            thread1.start();
            thread2.start();

            thread1.join();
            thread2.join();
        }
    }
}

