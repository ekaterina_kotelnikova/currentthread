package ru.task.catchup;

/**
 * Этот класс для демонстрации результатов изменения приоритета потоков
 *
 * @author Kotelnikova E.V. group15it20
 */
public class Main extends Thread {

    public static void main(String[] args) {
        Thread thread1 = Thread.currentThread();
        Thread thread2 = new Thread();
        thread1.setName("Улитка - ");
        thread1.setPriority(1);
        thread1.start();

        thread2.setName("Черепаха - ");
        thread2.setPriority(10);
        thread2.start();
    }

    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(this.getName() + i);
        }
        this.setPriority(this.getPriority() == 10 ? 1 : 10);
        for (int i = 100; i < 200; i++) {
            System.out.println(this.getName() + i);
        }
    }
}
