package ru.task.downloadimage;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Класс для скачивания картинок
 *
 * @author Kotelnikova E.V. group15it20
 */
public class DownloaderImage extends Thread {
    private String file;
    private String url;

    DownloaderImage(String file, String url) {
        this.file = file;
        this.url = url;
    }

    @Override
    public void run() {
        try {
            URL url = new URL(this.url);
            try (ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
                 FileOutputStream stream = new FileOutputStream(this.file)) {
                stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
