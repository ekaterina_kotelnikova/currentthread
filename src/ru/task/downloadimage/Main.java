package ru.task.downloadimage;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Класс для скачивания картинок с сайта
 *
 * @author Kotelnikova E.V. group15it20
 */

public class Main {

    private static final String INPUT_FILE = "src\\ru\\task\\downloadimage\\input.txt";
    private static final String OUTPUT_FILE = "src\\ru\\task\\downloadimage\\output.txt";
    private static final String PATH_TO_IMAGE = "src\\ru\\task\\downloadimage\\img";
    //private static final String DATA_URL = "\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")";
    private static final String DATA_URL = "(?<=src=\\\")[^\\\"]*(([jJ][pP][gG]|[pP][nN][gG]))(?=\\\")";
    private static final int COUNT_OF_IMAGE = 3;


    public static void main(String[] args) throws InterruptedException, IOException {
        final long before = System.currentTimeMillis();
        URL url = findUrl();
        String source = findHTML(url);

        fillingOutputFile(findURLFromHTML(source, DATA_URL, COUNT_OF_IMAGE), OUTPUT_FILE);
        waitDownload(downloadImage(OUTPUT_FILE, PATH_TO_IMAGE));

        System.out.println("Загрузка завершена");

        final long after = System.currentTimeMillis();
        System.out.printf("прошло времени: %d ", (after - before));
    }

    /**
     * Метод для ожидания окончания работы потоков
     * @param threads массив потоков
     * @throws InterruptedException исключение прерываний
     */
    private static void waitDownload(ArrayList<Thread> threads) throws InterruptedException {
        for (Thread thread : threads) {
            thread.join();
        }
    }

    /**
     * Метод для нахождения URL-адресов сайта
     *
     * @return URL-адрес сайта
     */
    private static URL findUrl() {
        String urlStr;
        URL url = null;
        try (BufferedReader inFile = new BufferedReader(new FileReader(INPUT_FILE))) {
            while ((urlStr = inFile.readLine()) != null) {
                url = new URL(urlStr);
                return url;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * Метод возвращает HTML-код страницы
     *
     * @param url URL-адрес сайта
     * @return HTML-код
     * @throws IOException исключение ввода-вывода
     */
    private static String findHTML(URL url) throws IOException {
        String result;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            result = bufferedReader.lines().collect(Collectors.joining("\n"));
        }
        return result;
    }

    /**
     * Метод для нахождение URL-адреса сайта в HTML коде
     *
     * @param source       HTML код
     * @param dataURL      URL-адрес сайта
     * @param countOfImage количество песен для скачивания
     * @return массив, содержащий URL-адреса
     */
    private static ArrayList<String> findURLFromHTML(String source, String dataURL, int countOfImage) {
        Pattern email_pattern = Pattern.compile(dataURL);
        Matcher matcher = email_pattern.matcher(source);
        ArrayList<String> urls = new ArrayList<>(countOfImage);
        for (int i = 0; i < countOfImage & matcher.find(); i++) {
            //String url = findUrl() + matcher.group();
            //System.out.println(url);
            urls.add(matcher.group());
        }
        return urls;
    }

    /**
     * Метод для записи ссылок на скачивание в файл
     *
     * @param urls   массив URL-адресов
     * @param output результирующий файл
     * @throws IOException исключение ввода-вывода
     */
    private static void fillingOutputFile(ArrayList<String> urls, String output) throws IOException {
        try (BufferedWriter outFile = new BufferedWriter(new FileWriter(output))) {
            for (String url : urls) {
                outFile.write(url + "\r\n");
            }
        }
    }


    /**
     * Метод для одновременного скачивания музыки
     *
     * @param output      путь к результирующему файлу
     * @param pathToImage путь для скачивания
     * @return массив потоков
     */
    private static ArrayList<Thread> downloadImage(String output, String pathToImage) {
        ArrayList<Thread> threads = new ArrayList<>();
        try (BufferedReader imageFile = new BufferedReader(new FileReader(output))) {
            String image;
            int count = 0;
            try {
                while ((image = imageFile.readLine()) != null) {
                    DownloaderImage loader = new DownloaderImage(pathToImage + String.valueOf(count) + ".jpg", image);
                    loader.start();
                    threads.add(loader);
                    count++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return threads;
    }
}
