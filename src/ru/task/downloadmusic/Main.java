package ru.task.downloadmusic;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Класс для скачивания музыки с сайта
 *
 * @author Kotelnikova E.V. group15it20
 */

public class Main {
    private static final String INPUT_FILE = "src\\ru\\task\\downloadmusic\\input.txt";
    private static final String OUTPUT_FILE = "src\\ru\\task\\downloadmusic\\output.txt";
    private static final String PATH_TO_MUSIC = "src\\ru\\task\\downloadmusic\\music_";
    private static final String DATA_URL = "\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")";
    private static final int COUNT_OF_MUSIC = 3;


    public static void main(String[] args) throws InterruptedException, IOException {
        final long before = System.currentTimeMillis();
        URL url = findUrl();
        String source = findHTML(url);

        fillingOutputFile(findURLFromHTML(source, DATA_URL, COUNT_OF_MUSIC), OUTPUT_FILE);
        waitDownload(downloadMusic(OUTPUT_FILE, PATH_TO_MUSIC));

        System.out.println("Загрузка завершена");

        final long after = System.currentTimeMillis();
        System.out.printf("прошло времени: %d ", (after - before));
    }

    /**
     * Метод для ожидания окончания работы потоков
     *
     * @throws InterruptedException
     */
    private static void waitDownload(ArrayList<Thread> threads) throws InterruptedException {
        for (Thread thread : threads) {
            thread.join();
        }
    }

    /**
     * Метод для нахождения URL-адресов сайта
     *
     * @return URL-адрес сайта
     */
    private static URL findUrl() {
        String urlStr;
        URL url = null;
        try (BufferedReader inFile = new BufferedReader(new FileReader(INPUT_FILE))) {
            while ((urlStr = inFile.readLine()) != null) {
                url = new URL(urlStr);
                return url;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * Метод возвращает HTML-код страницы
     *
     * @param url URL-адрес сайта
     * @return HTML-код
     * @throws IOException исключение ввода-вывода
     */
    private static String findHTML(URL url) throws IOException {
        String result;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            result = bufferedReader.lines().collect(Collectors.joining("\n"));
        }
        return result;
    }

    /**
     * Метод для нахождение URL-адреса сайта в HTML коде
     *
     * @param source       HTML код
     * @param dataURL      URL-адрес сайта
     * @param countOfMusic количество песен для скачивания
     * @return массив, содержащий URL-адреса
     */
    private static ArrayList<String> findURLFromHTML(String source, String dataURL, int countOfMusic) {
        Pattern email_pattern = Pattern.compile(dataURL);
        Matcher matcher = email_pattern.matcher(source);
        ArrayList<String> urls = new ArrayList<>(countOfMusic);
        for (int i = 0; i < countOfMusic & matcher.find(); i++) {
            urls.add(matcher.group());
        }
        return urls;
    }

    /**
     *
     */
    /**
     * Метод для записи ссылок на скачивание в файл
     *
     * @param urls   массив URL-адресов
     * @param output результирующий файл
     * @throws IOException исключение ввода-вывода
     */
    private static void fillingOutputFile(ArrayList<String> urls, String output) throws IOException {
        try (BufferedWriter outFile = new BufferedWriter(new FileWriter(output))) {
            for (String url : urls) {
                outFile.write(url + "\r\n");
            }
        }
    }


    /**
     * Метод для одновременного скачивания музыки
     *
     * @param output      путь к результирующему файлу
     * @param pathToMusic путь для скачивания
     * @return массив потоков
     */
    private static ArrayList<Thread> downloadMusic(String output, String pathToMusic) {
        ArrayList<Thread> threads = new ArrayList<>();
        try (BufferedReader musicFile = new BufferedReader(new FileReader(output))) {
            String music;
            int count = 0;
            try {
                while ((music = musicFile.readLine()) != null) {
                    DownloaderMusic downloader = new DownloaderMusic(pathToMusic + String.valueOf(count) + ".mp3", music);
                    downloader.start();
                    threads.add(downloader);
                    count++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return threads;
    }
}
