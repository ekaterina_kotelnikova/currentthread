Это репозиторий содержит пакеты:
demo, который содержит демонстрационные классы
            currentthread - для рассмотрения методов текущего потока
            eggorchicken
            hellorunnable
            hellothread
            interference
            runexample
            seriesrunexample
task, который содержит решённые задачи
            catchup - догонялки между потоками с помощью изменения приоритетов
            copiyingfiles - копирование данных из одного файла в другой
            readerthread - построчная запись из двух исходных файлов в один файл
            downloadMusic - скачивание музыки
            downloadImage - скачивание картинок

